import React, { lazy, Suspense } from 'react';
import './App.css';
import { PageHome } from './features/home/PageHome';
import { PageCognitive } from './features/cognitive/PageCognitive';
import { PageUsers } from './features/users/PageUsers';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import { Navbar } from './core/components/Navbar';
import { PageUserDetails } from './features/user-details/PageUserDetails';
import { PageLogin } from './features/login/PageLogin';
import { PageAdmin } from './features/admin/PageAdmin';
import { PrivateRoute } from './core/auth/PrivateRoute';
import { Interceptor } from './core/auth/Interceptor';
const PageSettings = lazy(() => import('./features/settings/PageSettings'))

function App() {

  return (
    <div>

      <BrowserRouter>
        <Suspense fallback={<div>loading....</div>}>
          <Interceptor />
          <Route path="*" component={Navbar} />

          <div className="container">
            <Switch>
              <Route path="/cognitive">
                <PageCognitive />
              </Route>
              <Route path="/home">
                <PageHome />
              </Route>
              <Route path="/settings">
                <PageSettings />
              </Route>
              <PrivateRoute path="/admin" color="red">
                <PageAdmin />
              </PrivateRoute>
              <Route path="/login" component={PageLogin}>
              </Route>
              <PrivateRoute path="/users" exact>
                <PageUsers />
              </PrivateRoute>

              <Route path="/users/:id" component={PageUserDetails} exact/>

              {/*
              <Route path="/users/:id/xyz" exact>
                <div>Sono qui</div>
              </Route>
              */}
              <Route path="*">
                <Redirect to="home" />
              </Route>
            </Switch>
          </div>
        </Suspense>
      </BrowserRouter>
    </div>
  );
}

export default App;
