import React  from 'react';
import { NavLink, useHistory } from 'react-router-dom';
import { signOut } from '../auth/authentication.service';
import { IfLogged } from '../auth/IfLogged';

export const Navbar: React.FC = () => {
  const history = useHistory();

  console.log('render')
  function logoutHandler() {
    signOut();
    history.push('/login/signin')
  }

  return <div>

    <nav className="navbar navbar-expand navbar-light bg-light">
      <div className="navbar-brand">
        <NavLink to="/login">React Training</NavLink>
      </div>


      <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="navbar-nav">
          <li className="nav-item">
            <NavLink
              activeClassName="active"
              className="nav-link"
              to="/cognitive">Cognitive</NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              activeClassName="active"
              className="nav-link"
              to="/home">Home</NavLink>
          </li>
          <IfLogged>
            <li className="nav-item" >
              <NavLink
                activeClassName="active"
                className="nav-link"
                to="/admin">ADmin</NavLink>
            </li>
          </IfLogged>
          <li className="nav-item">
            <NavLink
              activeClassName="active"
              className="nav-link"
              to="/settings">Settings</NavLink>
          </li>
          <IfLogged>
            <li className="nav-item">
              <NavLink
                activeClassName="active"
                className="nav-link"
                to="/users">Users</NavLink>
            </li>
          </IfLogged>

          <li className="nav-item" onClick={logoutHandler}>
            <div className="nav-link">Logout</div>
          </li>
        </ul>
      </div>
    </nav>

  </div>
};
