import Axios from "axios";
import { Auth } from './auth';
import { getItemFromLocalStorage, removeFromLocalStorage, setItemInLocalStorage } from './localstorage.helper';

export interface Credentials {
  username: string;
  password: string;
}


export function signIn(params: Credentials) {
  return Axios.get<Auth>('http://localhost:3001/login')
    .then(res => {
      console.log(res.data.accessToken)
      setItemInLocalStorage('token', res.data.accessToken);
    })
}

export function signOut() {
  removeFromLocalStorage('token')
}

export function isLogged(): boolean {
  return !!getItemFromLocalStorage('token')
}
/*

Axios.interceptors.request.use( (config) => {
  console.log('request', config)
  return {
    ...config,
    headers: {
      ...config.headers,
      'Authentication-JWT': 'bearer ' + getItemFromLocalStorage('token')
    }
  };
});

// handle errors
Axios.interceptors.response.use( (response) => {
  console.log('response', response)
  return response;
}, function (error) {
  // Any status codes that falls outside the range of 2xx cause this function to trigger
  console.log('ERROR', error)
  return Promise.reject(error);
});
*/
