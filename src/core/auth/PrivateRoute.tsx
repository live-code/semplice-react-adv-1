import React from 'react';
import { Route, Redirect, RouteProps } from 'react-router-dom';
import { isLogged } from './authentication.service';

export const PrivateRoute: React.FC<RouteProps & { color?: string}> = ({ children, color, ...rest }) => {
// export function PrivateRoute(props: RouteProps & { children: React.ReactNode}) {
  // console.log(color); // non serve a nulla :D
  return <Route {...rest} >
    {
      isLogged() ?
        children :
        <Redirect to={{ pathname: 'login/signin'}} />
    }
  </Route>
}
