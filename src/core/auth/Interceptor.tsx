import React  from 'react';
import { useInterceptor } from './useInterceptor';

export const Interceptor: React.FC = () => {
  const { error, cleanError } = useInterceptor();

  return <div>
    { error && <div className="alert alert-danger">
      problemi!
      <i className="fa fa-times" onClick={cleanError} />
    </div>}

  </div>
};
