import { useEffect, useState } from 'react';
import Axios from 'axios';
import { getItemFromLocalStorage } from './localstorage.helper';

export const useInterceptor = () => {
  const [request, setRequest] = useState<any>(null);
  const [error, setError] = useState<boolean>(false);

  useEffect(() => {
    console.log('useInterceptor')
    Axios.interceptors.request.use( (config) => {
      const req = {
        ...config,
        headers: {
          ...config.headers,
          'Authentication-JWT': 'bearer ' + getItemFromLocalStorage('token')
        }
      };
      setRequest(req)
      setError(false);

      return req;
    });

    // handle errors
    Axios.interceptors.response.use( (response) => {
      return response;
    }, function (error) {
      // Any status codes that falls outside the range of 2xx cause this function to trigger
      setError(true);
      return Promise.reject(error);
    });

  }, [])

  const cleanError = () => {
    setError(false)
  }

  return {
    request,
    error,
    cleanError
  }
}
