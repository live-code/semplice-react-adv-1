export function getItemFromLocalStorage(key: string) {
  return localStorage.getItem(key)
}
export function setItemInLocalStorage(key: string, value: string) {
  return localStorage.setItem(key, value)
}
export function removeFromLocalStorage(key: string) {
  return localStorage.removeItem(key);
}
