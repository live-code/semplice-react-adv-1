import React from 'react';
import { Route, Link, RouteComponentProps } from 'react-router-dom';
import { SignIn } from './components/SignIn';

export const PageLogin: React.FC<RouteComponentProps> = ({ match }) => {
  return <div>
    <Link to={match.path + '/signin'}>Sign in</Link> |
    <Link to={match.path + '/lostpass'}>Lost pass</Link> |
    <Link to={match.path + '/registration'}>Regi</Link>

    <Route path={match.path + '/:sectionName'} component={BreadCrumbs} />

    <Route path={match.path + '/signin'} component={SignIn} />

    <Route path={'/login/lostpass'}>
      <h1>Lost pass</h1>
    </Route>

    <Route path={'/login/registration'}>
      <h1>Registration</h1>
    </Route>

    <Route path={'/login'} exact>
      <h1>Welcome to my app</h1>
    </Route>

  </div>
};

type BreadCrumbsParams = { sectionName: string }
const BreadCrumbs = ({ match }: RouteComponentProps<BreadCrumbsParams>) => {
  return <div>
    <em>login / {match.params.sectionName}</em>
  </div>
}
