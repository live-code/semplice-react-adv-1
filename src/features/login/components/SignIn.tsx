import React  from 'react';
import { useHistory } from 'react-router';
import { signIn } from '../../../core/auth/authentication.service';

export const SignIn: React.FC = () => {
  const history = useHistory()

  function signHandler() {
    signIn({ username: 'a', password: 'b'})
      .then(() => {
        history.push('/admin')
      })
  }

  return <div>
    <h1>Login</h1>
    <input type="text"/>
    <input type="text"/>
    <button onClick={signHandler}>SIGNIN</button>
  </div>
};
