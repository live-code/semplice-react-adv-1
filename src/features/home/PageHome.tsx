import React, { useEffect, useState } from 'react';

export const PageHome: React.FC = () => {
  const [count, setCount] = useState(10);

  useEffect(function() {
    const id = setInterval(function() {
      console.log(`Count is: ${count}`); // Count is: 0
    }, 2000);

    return () => {
      clearInterval(id)
    }
  }, [count]);

  return (
    <div>
      {count}
      <button onClick={() => setCount(count + 1) }>
        Increase
      </button>
    </div>
  );
};
