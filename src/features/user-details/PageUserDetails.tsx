import React, { useEffect, useState } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import Axios from 'axios';
import { User } from '../users/model/user';

type RouterParams = { id: string};

export const PageUserDetails: React.FC<RouteComponentProps<RouterParams>> = ({ match }) => {
// export function PageUserDetails({ match }: RouteComponentProps<RouterParams>) {
  const [user, setUser] = useState<User>();

  useEffect(() => {
    Axios.get<User>(`https://jsonplaceholder.typicode.com/users/${match.params.id}`)
      .then(res => setUser(res.data));

    console.log('render')
    return () => {
      console.log('clean up')
    }
  }, [match.params.id]);

  return <div>
    <h1>{user?.id}</h1>
    <h1>{user?.name}</h1>
  </div>
};
