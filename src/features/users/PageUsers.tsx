import React, { useEffect, useState } from 'react';
import { User } from './model/user';
import Axios from 'axios';
import { NavLink } from 'react-router-dom';

export const PageUsers: React.FC = () => {
  const [users, setUsers] = useState<User[]>();

  useEffect(() => {
    console.log('Page users')
    Axios.get<User[]>('https://jsonplaceholder.typicode.com/users/')
      .then(res => setUsers(res.data))
  }, [])
  return <div>
    {
      users?.map((user: User) => {
        return (
          <NavLink to={'users/' + user.id} key={user.id}>
             <li className="list-group-item">{user.name}</li>
          </NavLink>
        )
      })
    }
  </div>
};
