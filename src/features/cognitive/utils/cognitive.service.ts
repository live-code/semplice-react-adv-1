import Axios from 'axios';
import { CognitiveSentiment, CognitiveTranslation } from '../model/translation-service';

export const doTranslate = (text: string) => {
  return Axios.post<CognitiveTranslation[]>(
    'https://api-eur.cognitive.microsofttranslator.com/translate',
    [{ text }],
    {
      params: { to: 'en', 'api-version': '3.0'},
      headers: {
        'Ocp-Apim-Subscription-Key': '1d0512b302234f14a23583410fc8986f',
        'Ocp-Apim-Subscription-Region': 'westeurope',
        'Content-Type': 'application/json',
      }
    }
  )
    .then(res => res.data[0].translations[0].text)
}
export const doSentiment = (text: string) => {
  return Axios.post<CognitiveSentiment>(
    'https://fblabdevtextanalytics.cognitiveservices.azure.com/text/analytics/v2.0/sentiment',
    { documents: [{ id: 1, text}]},
    {
      params: { to: 'en', 'api-version': '3.0'},
      headers: {
        'Ocp-Apim-Subscription-Key': '07b9d19428b74ddcb3615af8e64b0534',
        'Ocp-Apim-Subscription-Region': 'westeurope',
        'Content-Type': 'application/json',
      }
    }
  )
    .then(res => res.data.documents[0].score)
}
