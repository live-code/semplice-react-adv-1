import React, { useState } from 'react';
import { doTranslate } from './utils/cognitive.service';
import { CognitiveResult } from './components/CognitiveResult';
import { CognitiveForm } from './components/CognitiveForm';

export function PageCognitive() {
  const [data, setData] = useState<string>('');

  const onSubmit = (text: string) => {
    doTranslate(text)
      .then(translated => setData(translated))
  }

  return (
    <div className="container">
      <h1>React Sentiment Translator</h1>
      <CognitiveForm onSubmitForm={onSubmit} />
      <CognitiveResult translation={data} />
    </div>
  );
}
