import React, { useEffect, useState } from 'react';
import { doSentiment } from '../utils/cognitive.service';
import css from './SentimentBar.module.css';

interface SentimentBarProps {
  text: string
}

export const SentimentBar: React.FC<SentimentBarProps> = props => {
  const [value, setValue] = useState<number>(0)

  useEffect(() => {
    if(props.text) {
      doSentiment(props.text)
        .then(sentimentValue => setValue(sentimentValue))
    }
  }, [props.text]);

  const backgroundColor =  value < 0.5 ? 'red' : 'green';
  const width = (value * 100) + '%'

  return <div>
      <div
      className={'progress ' + css.progressAnimation}
      style={{ backgroundColor, width }}
    />
  </div>
}
