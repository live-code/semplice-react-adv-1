import React, { ChangeEvent, FormEvent, useState } from 'react';
import { doTranslate } from '../utils/cognitive.service';

interface CognitiveFormProps {
  onSubmitForm: (text: string) => void
}

export const CognitiveForm: React.FC<CognitiveFormProps> = (props) => {
  const [formState, setFormState] = useState<string>('ciao mondo');

  function onChangeHandler(event: ChangeEvent<HTMLTextAreaElement>) {
    setFormState(event.currentTarget.value)
  }


  const onSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    doTranslate(formState)
      .then(translated => props.onSubmitForm(translated))
  }


  return (
    <form onSubmit={onSubmit}>
        <textarea
          rows={3}
          placeholder="Insert text"
          className="form-control"
          onChange={onChangeHandler}
          value={formState}
        />
      <button type="submit" className="btn btn-primary">Translate</button>
    </form>
  )
};
