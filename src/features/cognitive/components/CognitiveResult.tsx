import { SentimentBar } from './SentimentBar';
import React from 'react';

interface CognitiveResultProps {
  translation: string;
}

export const CognitiveResult: React.FC<CognitiveResultProps> = ({ translation }) => {
  return (
    <>
      <textarea rows={3} value={translation} className="form-control" readOnly={true} />
      <SentimentBar text={translation} />
    </>
  )
}
