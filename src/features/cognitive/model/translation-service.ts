// translate
export interface DetectedLanguage {
  language: string;
  score: number;
}

export interface Translation {
  text: string;
  to: string;
}

export interface CognitiveTranslation {
  detectedLanguage: DetectedLanguage;
  translations: Translation[];
}

// sentiment
export interface Document {
  id: string;
  score: number;
}

export interface CognitiveSentiment {
  documents: Document[];
  errors: any[];
}
